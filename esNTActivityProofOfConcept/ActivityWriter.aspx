﻿<%@ Page Language="C#" Inherits="esNTActivityProofOfConcept.ActivityWriter" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>ActivityWriter</title>
    <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     
     

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
    <div class="row">
    <form id="form1" runat="server" class="col s12">

    <br>
    <br>

                
      <div class="row">     
	
         <div class="input-field col s6">
          <input placeholder="Enter your NT username" id="ntUserName" type="text" class="validate">
          <label for="first_name">Username</label>
        </div>

        <br>
        <br>

      

    </div>      
        <button class="btn submit waves-effect waves-light" type="submit" name="action">Write Activity
            <i class="material-icons right">send</i>
       </button>
	</form>
   </div>
        
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
            $(document).ready(function($){

               


            $('.submit').on('click', function (e) {
            
                 e.preventDefault();
            
                 var dataToSend = JSON.stringify({
                    'ntUserName': $('#ntUserName').val(),
                    });

                    $.ajax({
                        type: "POST",
                        url: "ActivityWriter.aspx/WriteiMISActivityWithNTUserName",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: dataToSend,
                        success: function (response) {
                            if (null != response.d && response.d.length == 1) {
                                console.log("Able to call method");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var obj = JSON.parse(jqXHR.responseText);
                            console.log(obj.Message);
                            console.log(jqXHR.statusText);
                            toastr.error(obj.Message);

                        }
                    });
                });

            });
            
    </script>    
</body>
</html>
