﻿using Asi.Soa.Core.DataContracts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;



namespace esNTActivityProofOfConcept
{
    public class Activity
    {
        public String userId { get; set; }
        public String activityType { get; set; }
        public DateTime transactionDate { get; set; }
        public DateTime thruDate { get; set; }
        public String productCode { get; set; }
        public String description { get; set; }
        public String note { get; set; }
        public String sourceCode { get; set; }
        public String category { get; set; }
        public String otherCode { get; set; }
        public String amount { get; set; }
        public String uf1 { get; set; }
        public String uf2 { get; set; }
        public String uf3 { get; set; }
        public String uf4 { get; set; }
        public String otherId { get; set; }
        public Decimal units { get; set; }

        public GenericEntityData getiMISActivity()
        {
            IdentityData identity = new IdentityData();
            identity.EntityTypeName = "Party";
            Collection<string> list = new Collection<string>();
            list.Add(userId);
            identity.IdentityElements = list;

            GenericPropertyDataCollection props = new GenericPropertyDataCollection();
            GenericPropertyData prop1 = new GenericPropertyData("ACTIVITY_TYPE", activityType);
            props.Add(prop1);
            prop1 = new GenericPropertyData("PartyId", userId);
            props.Add(prop1);
            if (transactionDate != null)
            {
                prop1 = new GenericPropertyData("TRANSACTION_DATE", Convert.ToDateTime(transactionDate));
                props.Add(prop1);
            }
            if (thruDate != null)
            {
                prop1 = new GenericPropertyData("THRU_DATE", Convert.ToDateTime(thruDate));
                props.Add(prop1);
            }
            prop1 = new GenericPropertyData("PRODUCT_CODE", productCode);
            props.Add(prop1);
            prop1 = new GenericPropertyData("UF_4", uf4);
            props.Add(prop1);
            prop1 = new GenericPropertyData("DESCRIPTION", description);
            props.Add(prop1);
            prop1 = new GenericPropertyData("NOTE", note);
            props.Add(prop1);
            prop1 = new GenericPropertyData("SOURCE_CODE", sourceCode);
            props.Add(prop1);
            prop1 = new GenericPropertyData("CATEGORY", category);
            props.Add(prop1);
            prop1 = new GenericPropertyData("OTHER_CODE", otherCode);
            props.Add(prop1);
            prop1 = new GenericPropertyData("UF_1", uf1);
            props.Add(prop1);
            prop1 = new GenericPropertyData("UNITS", units);
            props.Add(prop1);
            if (uf2 != null && uf2 != "")
            {
                prop1 = new GenericPropertyData("UF_2", uf2);
                props.Add(prop1);
            }
            if (uf3 != null && uf3 != "")
            {
                prop1 = new GenericPropertyData("UF_3", uf3);
                props.Add(prop1);
            }
            if (amount != null && amount != "")
            {
                prop1 = new GenericPropertyData("AMOUNT", amount);
                props.Add(prop1);
            }
            if (otherId != null && otherCode != "")
            {
                prop1 = new GenericPropertyData("OTHER_ID", otherId);
                props.Add(prop1);
            }
            GenericEntityData ge = new GenericEntityData();
            ge.EntityTypeName = "Activity";
            ge.PrimaryParentEntityTypeName = "Party";
            ge.PrimaryParentIdentity = identity;
            ge.Properties = props;

            return ge;
        }
    }

}
