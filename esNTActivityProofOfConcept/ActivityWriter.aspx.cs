﻿extern alias newton;
using System;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
//using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Web.Http;
using System.Web.Http.Description;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace esNTActivityProofOfConcept
{



        public class Link
        {
            public string Class { get; set; }
            public string href { get; set; }
            public string ntiid { get; set; }
            public string rel { get; set; }
            public string title { get; set; }
            public string method { get; set; }
        }

        public class GeographicalLocation
        {
            public string City { get; set; }
            public string Class { get; set; }
            public string Country { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
            public string MimeType { get; set; }
            public string State { get; set; }
        }

        public class MostRecentSession
        {
            public string Class { get; set; }
            public GeographicalLocation GeographicalLocation { get; set; }
            public string MimeType { get; set; }
            public int SessionEndTime { get; set; }
            public int SessionID { get; set; }
            public int SessionStartTime { get; set; }
            public string UserAgent { get; set; }
            public string Username { get; set; }
        }

        public class Contexts
        {
            public List<string> Items { get; set; }
        }

        public class Report
        {
            public string Class { get; set; }
            public string MimeType { get; set; }
            public Contexts contexts { get; set; }
            public string description { get; set; }
            public string href { get; set; }
            public string name { get; set; }
            public string rel { get; set; }
            public List<string> supported_types { get; set; }
            public string title { get; set; }
        }
        
        public class ExternalIds
        {
            public string iMISID { get; set; }
        }


        public class Item
        {
            public string Class { get; set; }
            public string ContainerId { get; set; }
            public double CreatedTime { get; set; }
            public string Creator { get; set; }
            public string ID { get; set; }
            public List<Link> Links { get; set; }
            public string MimeType { get; set; }
            public MostRecentSession MostRecentSession { get; set; }
            public string NTIID { get; set; }
            public string NonI18NFirstName { get; set; }
            public string NonI18NLastName { get; set; }
            public string OID { get; set; }
            public List<Report> Reports { get; set; }
            public string Username { get; set; }
            public object about { get; set; }
            public object affiliation { get; set; }
            public string alias { get; set; }
            public object avatarURL { get; set; }
            public object backgroundURL { get; set; }
            public string containerId { get; set; }
            public object description { get; set; }
            public object education { get; set; }
            public string email { get; set; }
            public ExternalIds external_ids { get; set; }
            public object facebook { get; set; }
            public object googlePlus { get; set; }
            public object home_page { get; set; }
            public string href { get; set; }
            public object instagram { get; set; }
            public object interests { get; set; }
            public int lastLoginTime { get; set; }
            public object linkedIn { get; set; }
            public object location { get; set; }
            public object positions { get; set; }
            public string realname { get; set; }
            public object role { get; set; }
            public object twitter { get; set; }
        }

        public class User
        {
            public List<Item> Items { get; set; }
            public int __invalid_name__Last_Modified { get; set; }
            public string href { get; set; }
        }


    public partial class ActivityWriter : System.Web.UI.Page
    {

        [WebMethod]
        public static User WriteiMISActivityWithNTUserName(string ntUserName)
        {
            HttpClient httpClient = new HttpClient();
            User user = new User();
            string iMISID = "";
            //var authArray = 
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", "ZW5zeW5jLmJyaWRnZTpkN2UyM2FzZEpEUzgj");
            httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
            HttpResponseMessage userResponse = httpClient.GetAsync("https://ensync-corp-alpha.nextthought.com/dataserver2/ResolveUser/" + ntUserName).Result;
            if (userResponse.IsSuccessStatusCode)
            {
                // dynamic userJSON = JValue.Parse(userResponse.ToString());
                //


                var jsonString = userResponse.Content.ReadAsStringAsync();
                jsonString.Wait();
                user = JsonConvert.DeserializeObject<User>(jsonString.Result);
                foreach (Item item in user.Items)
                    
                {
                    if (item.external_ids.iMISID != null)
                        iMISID = item.external_ids.iMISID;
                }



                Task task = WriteiMISActivity(iMISID);


                return user;
                //User user = userResponse.Content.ReadAsStringAsync<User>();
                //return user;
                //var userArray = userJSON.Items as JArray;
                //var stronglyTyped = userArray.To
                //return "success";
            }
            else
                return user;

        }

        //[HttpPost]
        //(typeof(bool))]
        public static async Task WriteiMISActivity(string iMISID)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = await client.PostAsync("https:/ensync39.ensync-corp.com/Asi.Scheduler_web/Token",
                                                                   new FormUrlEncodedContent(
                                                                            new[]
            {
                new KeyValuePair<string,string>("grant_type","password"),
                new KeyValuePair<string, string>("username", "manager"),
                new KeyValuePair<string, string>("password","newmanager234")
            }));



            if(responseMessage.IsSuccessStatusCode)
            {
                //getting a token
                //might not work to get to access token level
                string token = responseMessage.Content.ReadAsStringAsync().Result;

                //handle conflict between Newtonsoft and System.Net.Http.Formatting - done by using extern
                var formatter = new JsonMediaTypeFormatter
                {
                    SerializerSettings = new JsonSerializerSettings()
                    {
                        TypeNameHandling = TypeNameHandling.Objects
                    }
                };


                //creating activity object
                Activity activity = new Activity();
                activity.userId = iMISID;
                activity.activityType = "GIFT";
                activity.uf4 = "NT Activity";
                      

                //making an activity call with token
                client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                responseMessage = await client.GetAsync("https:/ensync39.ensync-corp.com/Asi.Scheduler_web/api/Activity?PartyId=" + iMISID + "&Activity_Type=GIFT");
                if(responseMessage.IsSuccessStatusCode){
                    client = new HttpClient();
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                    responseMessage = await client.PostAsync("https:/ensync39.ensync-corp.com/Asi.Scheduler_web/api/Activity", activity.getiMISActivity(), formatter, CancellationToken.None);
                    if (responseMessage.IsSuccessStatusCode)
                        return;
                    else
                        throw new Exception("There is an error");
                
                        
                }
            }

        }

    }


}
